## What's up with this project?

This project demonstrates the difference between Jest's sandboxed environment and a Mocha environment.

1. Run `yarn` to install dependencies
2. Run `yarn test-jest`

You'll notice that the test passes. Look at how `singleton.test.js` and `singleton_2.test.js` both effect the
state of a `singleton.js` module. But! Because Jest isolates each test file, the state change of one test file
doesn't effect a separate file.

3. Run `yarn test-mocha`

You'll notice that the test fails. This is because Mocha **does not** isolate test files by default.
