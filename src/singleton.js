const singleton = {
  count: 0,
  next() {
    this.count += 1;
  },
};

module.exports = {
  singleton,
};
