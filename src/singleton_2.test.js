const assert = require("assert");
const { singleton } = require("./singleton");

describe("singleton_2", () => {
  it("starts with count 0", () => {
    assert.equal(singleton.count, 0);
  });

  it("goes to the next value", () => {
    singleton.next();

    assert.equal(singleton.count, 1);
  });
});
